#Papers library for SAM Totem.com.pl
> System Asset Management for company [TOTEM.COM.PL](https://www.totem.com.pl)

This is a package made exclusively for private project for asset management in IT Operations.
It contains CRUDs for papers, weights, and volumes which are used by SAM.

It is built on [Laravel 5.5](http://laravel.com).

-----

## General System Requirements

| Requirement | Notes |
| --- | --- |
| PHP | >7.0.0 |
| MySQL | >5.7 |

## Translations

Package supports multi languages. Currently available is **pl** and **en**.
If you'd like to translate package into a language that we don't offer, simply let us know via email.

-----

## Author
* **Jan Rejnowski** - Junior programmer

## Release History
* 1.0.8
    * Added getSimplerPublicPapersForJson method in PaperRepo in order to get JSON with less parameters. 
* 1.0.7
    * Added Default attribute for PaperWeightVolume relation.
    * Fixed problems with Public Papers.
* 1.0.6
    * Added Value attribute to weight and volume.
    * Rebased tag.
* 1.1.5
    * Added all method implementation in Repos to fix problems with displaying lists
* 1.1.4
    * Added edit method to PaperRepository and used it in API edit route for paper
    * Added sorting by name in views lists
* 1.1.3
    * Added destroy method instead of delete in Repos
    * Changes in routes in order to get proper response while deleting records by API
* 1.1.2
    * Fixes to readme.md
    * Added final delete method in Repos
    * Changes in routes linked to changed delete method in Repos
* 1.1.1
    * Fixes to readme.md
    * Changes in api routes names
    * Changes in views
    * Added deleteById function to Repos
* 1.1.0
    * Added API routing for create/delete Weight and Volume and create/delete/edit Paper 
* 1.0.5
    * Fix in PaperRequest to allow adding paper with no weight and volume
* 1.0.4
    * Changes in routing(group paper outside from middleware)
    * Change in PapersSeeder
    * Changes in papers views(volume to name)
    * Change in config for drop-down menu
* 1.0.3
    * Changes in migration - field 'volume' in volumes table is now 'name'
    * Changes in VolumeSeeder 'volume' is now 'name'
    * Changes in Volume model, 'name' is now fillable, added method to get Text and Value Attribute
    * Changes in VolumeRequest volume/name change in validation
    * Changes in volumes views linked to the change of volume to name
    * Some fixes to readme.md
    * Change in getParamsAttribute method in Paper Model
    * Change in $hidden in Weight Model
    * New method getPapersForJson in PaperRepository
    * Minor changes in VolumeRepository
* 1.0.2
    * Fixed readme.md
    * Changes in config.php('route-prefix', 'route', 'key', 'icon-class')
    * New methods in PaperRepository getPublicPaper and getPublicPapers
    * Added PaperLibraryPermissionSeeder and changes related to it in Migration
    * Changes in pl.json related to permissions
    * Changes in views(switch-toggle, box-border)
* 1.0.1
    * Fixed readme.md
    * Added new method in PaperRepository to get the papers
    * Changed order in the drop-down menu
* 1.0.0
    *  Added drop-down menu for paper
    *  Added PaperRequest with validation
    *  Added config file
    *  Changes in routing, deleted route /list  
* Initial
    * First Initialize with Paper, Volume, Weight CRUDs
    
## Instructions
* Add package to your "require" in composer.json "require":
    
    ```
      "janrejnowski/papers-library": "dev-master"
    ``` 
        
* Add package to "repositories" in composer.json:
  
```php
      {
        "type": "vcs",
        "url":  "https://janrejnowski@bitbucket.org/janrejnowski/papers-library.git"
      }      
``` 

* Update your composer:
 
    ```
    $ composer update
    ```
    
* Add PapersLibraryServiceProvider to your 'providers' in config/app.php:

    ```
    JanRejnowski\PapersLibrary\PapersLibraryServiceProvider::class 
    ``` 

* Make migration:

    ```
    $ php artisan migrate
    ``` 

* You can publish files from package by using:

    ```
    $ php artisan vendor:publish --provider="JanRejnowski\PapersLibrary\PapersLibraryServiceProvider"
    ``` 
## Usage

* GET api/papers/ - get you all the public papers

* GET api/papers/simpler - get you all the public papers in simpler version

* GET api/papers/all - get you all the papers

* POST api/papers/create - you can create new paper
  
    ```
    name|string|required
    ```
  
    ```
    long_description|string|required
    ```
  
    ```
    slug|string|required
    ```
  
    ```
    public|numeric|(0-1)
    ```
  
    ```
    weight_volume|array
    ```
    
    ```
    a. weight_id|numeric
    ```
    
    ```
    b. volume_id|numeric
    ```
  
```json
  {
      "name": "",
      "long_description": "",
      "slug": "",
      "public": "",
      "weight_volume": 
      [
        { "weight_id": "" , "volume_id": ""},
        { "weight_id": "", "volume_id": ""}
      ]
  }
```
  
* PATCH api/papers/{id}/edit - you can edit paper with given id

    ```
      name|string|required
    ```
    
    ```
      long_description|string|required
    ```
    
    ```
      slug|string|required
    ```
    
    ```
      public|numeric|(0-1)
    ```
    
    ```
      weight_volume|array
    ```
    
    ```
      a. weight_id|numeric
    ```
    
    ```
      b. volume_id|numeric 
    ``` 
         
```json
  {
      "name": "",
      "long_description": "",
      "slug": "",
      "public": "",
      "weight_volume": 
      [
        { "weight_id": "" , "volume_id": ""},
        { "weight_id": "", "volume_id": ""}
      ]
  }
```
  
* DELETE api/papers/{id}/destroy - you can delete paper with given id

* POST api/papers/weights/create - you can add new weight
    
    ```
      name|numeric|required  
    ```
    
    ```
      value|numeric|required
    ```
    
```json
      {
          "name": "",
          "value": ""
      }
```
    
* DELETE api/papers/weights/{id}/destroy - you can delete weight with given id

* POST api/papers/volumes/create - you can add a new volume
    
    ```
      name|numeric|required  
    ```
    
    ```
      value|numeric|required
    ```
    
```json
      {
          "name": "",
          "value": ""
      }
```
    
* DELETE api/papers/volumes/{id}/destroy - you can delete volume with given id

