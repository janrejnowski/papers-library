<?php

namespace JanRejnowski\PapersLibrary\Requests\Weight;

use Illuminate\Foundation\Http\FormRequest;

class WeightRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        return [
            'name'  => 'required|numeric|unique:weights,name,' . $this->id,
            'value' => 'required|numeric'
        ];
    }
}
