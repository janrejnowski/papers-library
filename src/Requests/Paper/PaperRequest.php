<?php

namespace JanRejnowski\PapersLibrary\Requests\Paper;

use Illuminate\Foundation\Http\FormRequest;

class PaperRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() : bool
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() : array
    {
        if($this->request->get('weight_volume')) {
            $this->request->set('weight_volume', array_map('unserialize', array_unique(array_map('serialize', $this->weight_volume))));
            if ($this->default !== null){
                $default = $this->weight_volume[$this->default];
                $this->request->set('weight_volume', array_map(function($item) use ($default) {
                    $item['default'] = empty(array_diff_assoc($item, $default)) ? 1 : 0;
                    return $item;
                },$this->weight_volume));
            }
        }

        return [
            'name'              => 'required|string|max:50',
            'long_description'  => 'required|string',
            'slug'              => 'required|string|max:50'
        ];
    }
}
