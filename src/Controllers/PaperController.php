<?php

namespace JanRejnowski\PapersLibrary\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\MessageBag;
use JanRejnowski\PapersLibrary\Repositories\PaperRepository;
use JanRejnowski\PapersLibrary\Repositories\VolumeRepository;
use JanRejnowski\PapersLibrary\Repositories\WeightRepository;
use JanRejnowski\PapersLibrary\Requests\Paper\PaperRequest;

class PaperController extends Controller
{
    public function __construct(PaperRepository $repository, Redirector $redirect)
    {
        $this->model = $repository;
        $this->redirect = $redirect;
    }

    public function index() : \Illuminate\View\View
    {
        $data = $this->model->all();

        return view('papers-library::papers.index-static')->with('papers', $data);
    }

    public function create(WeightRepository $weightRepository, VolumeRepository $volumeRepository) : \Illuminate\View\View
    {
        return view('papers-library::papers.create')->with([
            'weights' => $weightRepository->all(),
            'volumes' => $volumeRepository->all()
        ]);
    }

    public function edit(int $id = 0, WeightRepository $weightRepository, VolumeRepository $volumeRepository)
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->redirect->route('papers-library.paper.index')->with('error', $data->first('message'));
        }

        return view('papers-library::papers.edit')->with([
            'paper' => $data,
            'weights' => $weightRepository->all(),
            'volumes' => $volumeRepository->all()
        ]);
    }

    public function update(PaperRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request, $id);

        return $this->redirect->route('papers-library.paper.index')->with('success', __('Paper :paper updated successfully.', ['paper' => $data->name]));
    }

    public function store(PaperRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request);

        return $this->redirect->route('papers-library.paper.index')->with('success', __('Paper :paper created successfully.', ['paper' => $data->name]));
    }

    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->redirect->route('papers-library.paper.index')->with('error', $data->first('message'));
        }

        $data->delete();

        return $this->redirect->route('papers-library.paper.index')->with('success', __('Paper :paper deleted successfully.', ['paper' => $data->name]));
    }
}