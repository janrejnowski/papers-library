<?php

namespace JanRejnowski\PapersLibrary\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\MessageBag;
use JanRejnowski\PapersLibrary\Repositories\WeightRepository;
use JanRejnowski\PapersLibrary\Requests\Weight\WeightRequest;

class WeightController extends Controller
{
    public function __construct(WeightRepository $repository, Redirector $redirect)
    {
        $this->model = $repository;
        $this->redirect = $redirect;
    }

    public function index() : \Illuminate\View\View
    {
        $data = $this->model->all();

        return view('papers-library::weights.index-static')->with('weights', $data);
    }

    public function create() : \Illuminate\View\View
    {
        return view('papers-library::weights.create');
    }

    public function store(WeightRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request);

        return $this->redirect->route('papers-library.weight.index')->with('success', __('Weight :name created successfully.', ['name' => $data->name]));
    }

    public function edit(int $id = 0)
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->redirect->route('papers-library.weight.index')->with('error', $data->first('message'));
        }

        return view('papers-library::weights.edit')->with([
            'weight' => $data
        ]);
    }

    public function update(WeightRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request, $id);

        return $this->redirect->route('papers-library.weight.index')->with('success', __('Weight :name updated successfully.', ['name' => $data->name]));
    }

    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->redirect->route('papers-library.weight.index')->with('error', $data->first('message'));
        }

        $data->delete();

        return $this->redirect->route('papers-library.weight.index')->with('success', __('Weight :name deleted successfully.', ['name' => $data->name]));
    }
}