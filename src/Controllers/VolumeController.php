<?php

namespace JanRejnowski\PapersLibrary\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\MessageBag;
use JanRejnowski\PapersLibrary\Repositories\VolumeRepository;
use JanRejnowski\PapersLibrary\Requests\Volume\VolumeRequest;

class VolumeController extends Controller
{
    public function __construct(VolumeRepository $repository, Redirector $redirect)
    {
        $this->model = $repository;
        $this->redirect = $redirect;
    }

    public function index() : \Illuminate\View\View
    {
        $data = $this->model->all();

        return view('papers-library::volumes.index-static')->with('volumes', $data);
    }

    public function create() : \Illuminate\View\View
    {
        return view('papers-library::volumes.create');
    }

    public function store(VolumeRequest $request) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request);

        return $this->redirect->route('papers-library.volume.index')->with('success', __('Volume :volume created successfully.', ['volume' => $data->name]));
    }

    public function edit(int $id = 0)
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->redirect->route('papers-library.volume.index')->with('error', $data->first('message'));
        }

        return view('papers-library::volumes.edit')->with([
            'volume' => $data
        ]);
    }

    public function update(VolumeRequest $request, int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->store($request, $id);

        return $this->redirect->route('papers-library.volume.index')->with('success', __('Volume :volume updated successfully.', ['volume' => $data->name]));
    }

    public function destroy(int $id = 0) : \Illuminate\Http\RedirectResponse
    {
        $data = $this->model->findId($id);

        if ($data instanceof MessageBag) {
            return $this->redirect->route('papers-library.volume.index')->with('error', $data->first('message'));
        }

        $data->delete();

        return $this->redirect->route('papers-library.volume.index')->with('success', __('Volume :volume deleted successfully.', ['volume' => $data->name]));
    }
}