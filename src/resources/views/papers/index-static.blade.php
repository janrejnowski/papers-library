@extends('papers-library::papers.panel')

@section('title', __('Paper'))

@section('content')
    <div class="row">
        <div id="DTAA" class="col-lg-9 col-md-8 col-sm-8 col-xs-7">
            <a class="btn btn-sm btn-success" href="{{ route('papers-library.paper.create') }}" title="{{__('Create New')}}"><i class="fa fa-plus-circle"></i> {{__('Create New')}}</a>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <table id="users-table" class="panel panel-primary table table-striped table-bordered dataTable" width="100%">
                <thead class="panel-heading">
                <tr>
                    <th>{{ __('Paper') }}</th>
                    <th>{{ __('Actions') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($papers as $paper)
                    <tr>
                        <td>{{ $paper->name }}</td>
                        <td>
                            <a class="btn btn-xs btn-warning" href="{{ route('papers-library.paper.edit', $paper->id) }}" title="{{__('Edit')}}"><i class="fa fa-pencil"></i></a>
                            <form method="post" action="{{ route('papers-library.paper.destroy', $paper->id) }}" style="display: inline;">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="button" class="btn btn-xs btn-danger"  title="{{__('Delete')}}"><i class="fa fa-times-circle"></i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@include('layout.dialog-delete')