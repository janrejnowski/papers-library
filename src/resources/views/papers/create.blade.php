@extends('papers-library::papers.panel')

@section('title', __('Create new paper'))

@section('content')
    <div class="row">
        <section class="content-header">
            <h1>{{ __('Create new paper') }}</h1>
            @component('layout.breadcrumb', [ 'data' => [ 'Paper' => 'papers-library.paper.index', 'Creating' => '' ] ])@endcomponent
        </section>
        <div class="col-xs-12">
            @component('layout.alert')
                @include('layout.errors')
            @endcomponent
            <div class="row">
                <div class="col-xs-12 col-lg-offset-3 col-lg-7">
                    <div class="box box-default">
                        <form class="form-horizontal" method="POST" action="{{ route('papers-library.paper.store') }}">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-3 control-label">{{__('Name')}}</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control required" id="name" name="name" placeholder="{{__('Paper name')}}" value="{{ old('name')}}" maxlength="50" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="long_description" class="col-sm-3 control-label">{{__('Description')}}</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control required" id="long_description" name="long_description" placeholder="{{__('Paper description')}}" value="{{ old('long_description')}}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="slug" class="col-sm-3 control-label">{{__('Slug')}}</label>
                                            <div class="col-sm-7">
                                                <input type="text" class="form-control required" id="slug" name="slug" placeholder="{{__('Paper slug')}}" value="{{ old('slug')}}" maxlength="50" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="public" class="col-sm-3 control-label">{{__('Public')}}</label>
                                            <div class="col-sm-7">
                                                <input id="public" class="switch-toggle" name="public" type="checkbox" value="1">
                                                <label for="public"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <hr>

                            <div id="value-box">
                                <div class="form-group hide">
                                    <label for="default[0]" class="col-xs-2 control-label">{{__('Default')}}</label>
                                    <div class="col-xs-1">
                                        <input id="default[0]" class="radio" name="default" type="radio">
                                    </div>
                                    <label for="weight[0]" class="col-xs-1 control-label">{{__('Weight')}}</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <select name="weight[0]" id="weight[0]" class="form-control">
                                                @foreach($weights as $weight)
                                                    <option value="{{ $weight->id }}">{{ $weight->name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="input-group-addon">{{ __('gsm') }}</span>
                                        </div>
                                    </div>
                                    <label for="volume[0]" class="col-xs-2 control-label">{{__('Volume')}}</label>
                                    <div class="col-xs-2">
                                        <select name="volume[0]" id="volume[0]" class="form-control">
                                            @foreach($volumes as $volume)
                                                <option value="{{ $volume->id }}">{{ $volume->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-1 text-center hidden-print">
                                        <button type="button" class="btn btn-danger btn-ms"><i class="fa fa-close"></i></button>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label for="default" class="col-xs-2 control-label">{{__('Default')}}</label>
                                <div class="col-xs-1">
                                    <input id="default" class="radio" name="default" type="radio">
                                </div>
                                <label for="weight_id" class="col-xs-1 control-label">{{__('Weight')}}</label>
                                <div class="col-xs-3">
                                    <div class="input-group">
                                        <select name="weight_id" id="weight_id" class="form-control">
                                            @foreach($weights as $weight)
                                                <option value="{{ $weight->id }}">{{ $weight->name }}</option>
                                            @endforeach
                                        </select>
                                        <span class="input-group-addon">{{ __('gsm') }}</span>
                                    </div>
                                </div>
                                <label for="volume_id" class="col-xs-2 control-label">{{__('Volume')}}</label>
                                <div class="col-xs-2">
                                    <select name="volume_id" id="volume_id" class="form-control">
                                        @foreach($volumes as $volume)
                                            <option value="{{ $volume->id }}">{{ $volume->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-xs-1 text-center hidden-print">
                                    <button type="button" class="btn btn-ms btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                            </div>

                            <div class="box-footer">
                                <button class="btn btn-primary pull-right"><i class="fa fa-check"></i> {{ __('Confirm') }}</button>
                                <a class="btn btn-danger" href="{{ route('papers-library.paper.index') }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts-inline')
    <script>
        $(function () {

            let $formBox = $('#value-box');
            $formBox.on('click', 'button.btn-danger', function (e) {
                e.preventDefault();
                $(this).closest('.form-group').remove();
            });

            $('button.btn-success').on('click', function (e) {
                e.preventDefault();
                let $html = $formBox.find('.form-group.hide'),
                    $clone = $html.clone(),
                    prev_id = $html.prev('.form-group').attr('data-id'),
                    current_id = parseInt(typeof prev_id !== "undefined" ? prev_id : '0')+1,
                    weight = 'weight_volume['+current_id+'][weight_id]',
                    volume = 'weight_volume['+current_id+'][volume_id]',
                    _default = 'weight_volume['+current_id+'][default]'
                ;

                console.log(typeof $html.prev('.form-group').attr('data-id'));
                $clone.attr('data-id', current_id);
                $clone.find('label[for="weight[0]"]').attr('for', weight);
                $clone.find('label[for="volume[0]"]').attr('for', volume);
                $clone.find('label[for="default[0]"]').attr('for', _default);
                $clone.find('select[name="weight[0]"]').attr({
                    'id': weight,
                    'name': weight
                }).val($('select#weight_id').val());
                $clone.find('select[name="volume[0]"]').attr({
                    'id': volume,
                    'name': volume
                }).val($('select#volume_id').val());
                $clone.find('input[id="default[0]"]').attr({
                    'id': _default,
                    'name': 'default'
                }).val(current_id);
                $html.before($clone.removeClass('hide'));
            })
        });
    </script>
@endpush