@extends('papers-library::weights.panel')

@section('title', __('Edit weight'))

@section('content')
    <div class="row">
        <section class="content-header">
            <h1>{{ __('Edit weight') }}</h1>
            @component('layout.breadcrumb', [ 'data' => [ 'Papers weights' => 'papers-library.weight.index', 'Editing' => '' ] ])@endcomponent
        </section>
        <div class="col-xs-12">
            @component('layout.alert')
                @include('layout.errors')
            @endcomponent
            <div class="row">
                <div class="col-xs-12 col-lg-offset-3 col-lg-6">
                    <div class="box box-default">
                        <form class="form-horizontal" method="POST" action="{{ route('papers-library.weight.update', $weight->id) }}">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-3 control-label">{{__('Weight')}}</label>
                                            <div class="col-sm-7">
                                                <input type="number" min="0" step="1" max="400" class="form-control required" id="name" name="name" placeholder="{{__('Paper weight')}}" value="{{ old('name') ?: $weight->name }}" maxlength="50" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="value" class="col-sm-3 control-label">{{__('Value')}}</label>
                                            <div class="col-sm-7">
                                                <input type="number" min="0" step="1" max="400" class="form-control required" id="value" name="value" placeholder="{{__('Value')}}" value="{{ old('value') ?: $weight->value }}" maxlength="50" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary pull-right"><i class="fa fa-check"></i> {{ __('Confirm') }}</button>
                                <a class="btn btn-danger" href="{{ route('papers-library.weight.index') }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection