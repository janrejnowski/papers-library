@extends('papers-library::volumes.panel')

@section('title', __('Create new volume'))

@section('content')
    <div class="row">
        <section class="content-header">
            <h1>{{ __('Create new volume') }}</h1>
            @component('layout.breadcrumb', [ 'data' => [ 'Papers volumes' => 'papers-library.volume.index', 'Creating' => '' ] ])@endcomponent
        </section>
        <div class="col-xs-12">
            @component('layout.alert')
                @include('layout.errors')
            @endcomponent
            <div class="row">
                <div class="col-xs-12 col-lg-offset-3 col-lg-6">
                    <div class="box box-default">
                        <form class="form-horizontal" method="POST" action="{{ route('papers-library.volume.store') }}">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label for="name" class="col-sm-3 control-label">{{__('Volume')}}</label>
                                            <div class="col-sm-7">
                                                <input type="number" min="0" step="0.01" max="100" class="form-control required" id="name" name="name" placeholder="{{__('Paper volume')}}" value="{{ old('name') }}" maxlength="50" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="value" class="col-sm-3 control-label">{{__('Value')}}</label>
                                            <div class="col-sm-7">
                                                <input type="number" min="0" step="0.01" max="100" class="form-control required" id="value" name="value" placeholder="{{__('Value')}}" value="{{ old('value') }}" maxlength="50" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button class="btn btn-primary pull-right"><i class="fa fa-check"></i> {{ __('Confirm') }}</button>
                                <a class="btn btn-danger" href="{{ route('papers-library.volume.index') }}"><i class="fa fa-arrow-left"></i> {{ __('Back') }}</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection