<?php

namespace JanRejnowski\PapersLibrary\Model;

use Illuminate\Database\Eloquent\Model;

class PaperWeightVolume extends Model
{
    protected $table = 'paper_weight_volume';
}