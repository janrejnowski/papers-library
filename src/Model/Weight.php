<?php

namespace JanRejnowski\PapersLibrary\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Weight
 * @package JanRejnowski\PapersLibrary\Model
 *
 * @method      \Illuminate\Database\Eloquent\Builder withTrashed()
 */
class Weight extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot',
        'volume'
    ];

    protected $fillable = [
        'name',
        'value',
        'active'
    ];

    public function volume() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Volume::class, 'paper_weight_volume')->withPivot('paper_id')->using(VolumeWeight::class);
    }

    public function paper() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Paper::class, 'paper_weight_volume')->withPivot('weight_id')->using(VolumeWeight::class);
    }

    public function getTextAttribute() : string
    {
        return $this->name;
    }


    public function getDefaultAttribute() : int
    {
        return $this->pivot->default;
    }
}
