<?php

namespace JanRejnowski\PapersLibrary\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Volume
 * @package JanRejnowski\PapersLibrary\Model
 *
 * @method      \Illuminate\Database\Eloquent\Builder withTrashed()
 */
class Volume extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot'
    ];

    protected $fillable = [
        'name',
        'value',
        'active'
    ];

    public function paper() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Paper::class, 'paper_weight_volume')->withPivot('weight_id')->using(VolumeWeight::class);
    }

    public function weight() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Weight::class, 'paper_weight_volume')->withPivot('volume_id')->using(VolumeWeight::class);
    }

    public function getTextAttribute() : string
    {
        return $this->name;
    }

    public function getSelectNameAttribute() : string
    {
        if ($this->name === $this->value) {
            return $this->name;
        }
        return "{$this->name} ({$this->value})";
    }
}
