<?php

namespace JanRejnowski\PapersLibrary\Model;

use Illuminate\Database\Eloquent\Relations\Pivot;

class VolumeWeight extends Pivot
{
    public function paper() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Paper::class);
    }

    public function volume() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Volume::class);
    }

    public function weight() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Weight::class);
    }
}
