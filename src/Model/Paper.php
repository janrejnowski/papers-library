<?php

namespace JanRejnowski\PapersLibrary\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Paper
 * @package JanRejnowski\PapersLibrary\Model
 *
 * @method      \Illuminate\Database\Eloquent\Builder withTrashed()
 */
class Paper extends Model
{
    use SoftDeletes;

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'pivot',
        'weight'
    ];

    protected $appends = [
        'params'
    ];

    protected $fillable = [
        'name',
        'short_description',
        'long_description',
        'public',
    ];

    public function weight() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Weight::class, 'paper_weight_volume')->withPivot(['volume_id', 'default'])->using(VolumeWeight::class);
    }

    public function volumes() : \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Volume::class, 'paper_weight_volume')->withPivot('weight_id')->using(VolumeWeight::class);
    }

    public function getParamsAttribute()
    {
        return $this->weight->unique()->mapWithKeys(function($item){
            return [$item->id => [
                'weight' => $item,
                'volumes' => $item->volume->where('pivot.paper_id', $item->pivot->paper_id)->where('pivot.weight_id', $item->pivot->weight_id)
                ]
            ];
        });
    }

    public function getPivotAttribute()
    {
        return PaperWeightVolume::where('paper_id', $this->id)->get();
    }

}
