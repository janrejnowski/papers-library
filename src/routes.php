<?php

Route::group([ 'middleware' => ['web', 'auth']], function (){

    Route::group(['prefix' => 'papers'], function (){

        Route::get('/','JanRejnowski\PapersLibrary\Controllers\PaperController@index')->name('papers-library.paper.index');
        Route::get('{id}/edit', 'JanRejnowski\PapersLibrary\Controllers\PaperController@edit')->name('papers-library.paper.edit');
        Route::patch('{id}/edit', 'JanRejnowski\PapersLibrary\Controllers\PaperController@update')->name('papers-library.paper.update');
        Route::get('create', 'JanRejnowski\PapersLibrary\Controllers\PaperController@create')->name('papers-library.paper.create');
        Route::post('create', 'JanRejnowski\PapersLibrary\Controllers\PaperController@store')->name('papers-library.paper.store');
        Route::delete('{id}/destroy','JanRejnowski\PapersLibrary\Controllers\PaperController@destroy')->name('papers-library.paper.destroy');

        Route::group(['prefix' => 'volumes', 'middleware' => ['web','auth']], function (){
            Route::get('/', 'JanRejnowski\PapersLibrary\Controllers\VolumeController@index')->name('papers-library.volume.index');
            Route::get('create', 'JanRejnowski\PapersLibrary\Controllers\VolumeController@create')->name('papers-library.volume.create');
            Route::post('create', 'JanRejnowski\PapersLibrary\Controllers\VolumeController@store')->name('papers-library.volume.store');
            Route::get('{id}/edit', 'JanRejnowski\PapersLibrary\Controllers\VolumeController@edit')->name('papers-library.volume.edit');
            Route::patch('{id}/edit', 'JanRejnowski\PapersLibrary\Controllers\VolumeController@update')->name('papers-library.volume.update');
            Route::delete('{id}/destroy', 'JanRejnowski\PapersLibrary\Controllers\VolumeController@destroy')->name('papers-library.volume.destroy');
        });

        Route::group(['prefix' => 'weights', 'middleware' => ['web', 'auth']], function (){
            Route::get('/', 'JanRejnowski\PapersLibrary\Controllers\WeightController@index')->name('papers-library.weight.index');
            Route::get('create', 'JanRejnowski\PapersLibrary\Controllers\WeightController@create')->name('papers-library.weight.create');
            Route::post('create', 'JanRejnowski\PapersLibrary\Controllers\WeightController@store')->name('papers-library.weight.store');
            Route::get('{id}/edit', 'JanRejnowski\PapersLibrary\Controllers\WeightController@edit')->name('papers-library.weight.edit');
            Route::patch('{id}/edit', 'JanRejnowski\PapersLibrary\Controllers\WeightController@update')->name('papers-library.weight.update');
            Route::delete('{id}/destroy', 'JanRejnowski\PapersLibrary\Controllers\WeightController@destroy')->name('papers-library.weight.destroy');
        });
    });
});

Route::group(['prefix' => 'api'], function (){

    Route::group(['prefix' => 'papers'], function (){

       Route::get('/', function (){
            return new \App\Http\Resources\Contracts\ApiCollection(
                (new JanRejnowski\PapersLibrary\Repositories\PaperRepository)->getPublicPapersForJson()
            );
       })->name('api.papers-library.paper.public');

        Route::get('simpler', function (){
            return new \App\Http\Resources\Contracts\ApiCollection(
                (new JanRejnowski\PapersLibrary\Repositories\PaperRepository)->getSimplerPublicPapersForJson()
            );
        })->name('api.papers-library.paper.public.simpler');

       Route::get('all', function (){
            return new \App\Http\Resources\Contracts\ApiCollection(
                (new JanRejnowski\PapersLibrary\Repositories\PaperRepository)->getPapersForJson()
            );
       })->name('api.papers-library.paper.all');

       Route::post('create', function (\Illuminate\Http\Request $request){
            return new \App\Http\Resources\Contracts\Api(
                (new JanRejnowski\PapersLibrary\Repositories\PaperRepository)->store($request)
            );
       })->name('api.papers-library.paper.create')->middleware('auth:api');

        Route::patch('{id}/edit', function (\Illuminate\Http\Request $request, $id = 0){
            return new \App\Http\Resources\Contracts\Api(
                (new JanRejnowski\PapersLibrary\Repositories\PaperRepository)->edit($request, $id)
            );
        })->name('api.papers-library.paper.edit')->middleware('auth:api');

        Route::delete('{id}/destroy', function ($id = 0){
            return new \App\Http\Resources\Contracts\Api(
                (new JanRejnowski\PapersLibrary\Repositories\PaperRepository)->destroy($id)
            );
        })->name('api.papers-library.paper.destroy')->middleware('auth:api');

        Route::group(['prefix' => 'weights'], function (){
            Route::post('create', function (\Illuminate\Http\Request $request){
                return new \App\Http\Resources\Contracts\Api(
                    (new JanRejnowski\PapersLibrary\Repositories\WeightRepository)->store($request)
                );
            })->name('api.papers-library.weight.create')->middleware('auth:api');

            Route::delete('{id}/destroy', function ($id = 0){
                return new \App\Http\Resources\Contracts\Api (
                    (new JanRejnowski\PapersLibrary\Repositories\WeightRepository)->destroy($id)
                );
            })->name('api.papers-library.weight.destroy')->middleware('auth:api');
        });

       Route::group(['prefix' => 'volumes'], function (){
           Route::post('create', function (\Illuminate\Http\Request $request){
              return new \App\Http\Resources\Contracts\Api (
                  (new JanRejnowski\PapersLibrary\Repositories\VolumeRepository)->store($request)
              );
           })->name('api.papers-library.volume.create')->middleware('auth:api');

           Route::delete('{id}/destroy', function ($id = 0){
               return new \App\Http\Resources\Contracts\Api (
                   (new JanRejnowski\PapersLibrary\Repositories\VolumeRepository)->destroy($id)
               );
           })->name('api.papers-library.volume.destroy')->middleware('auth:api');
       });
    });
});

