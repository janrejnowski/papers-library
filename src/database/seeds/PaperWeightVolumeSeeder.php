<?php

namespace JanRejnowski\PapersLibrary\Database\Seeds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaperWeightVolumeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'paper_id' => 1,
                'weight_id' => 1,
                'volume_id' => 17
            ],
            [
                'paper_id' => 1,
                'weight_id' => 2,
                'volume_id' => 17
            ],
            [
                'paper_id' => 1,
                'weight_id' => 3,
                'volume_id' => 17
            ],
            [
                'paper_id' => 1,
                'weight_id' => 4,
                'volume_id' => 17
            ],
            [
                'paper_id' => 1,
                'weight_id' => 5,
                'volume_id' => 17
            ],
            [
                'paper_id' => 1,
                'weight_id' => 7,
                'volume_id' => 17
            ],
            [
                'paper_id' => 1,
                'weight_id' => 8,
                'volume_id' => 17
            ],
            [
                'paper_id' => 1,
                'weight_id' => 10,
                'volume_id' => 17
            ],
            [
                'paper_id' => 2,
                'weight_id' => 3,
                'volume_id' => 9
            ],
            [
                'paper_id' => 2,
                'weight_id' => 4,
                'volume_id' => 10
            ],
            [
                'paper_id' => 2,
                'weight_id' => 5,
                'volume_id' => 11
            ],
            [
                'paper_id' => 2,
                'weight_id' => 6,
                'volume_id' => 11
            ],
            [
                'paper_id' => 2,
                'weight_id' => 8,
                'volume_id' => 12
            ],
            [
                'paper_id' => 2,
                'weight_id' => 10,
                'volume_id' => 13
            ],
            [
                'paper_id' => 2,
                'weight_id' => 11,
                'volume_id' => 14
            ],
            [
                'paper_id' => 2,
                'weight_id' => 12,
                'volume_id' => 16
            ],
            [
                'paper_id' => 3,
                'weight_id' => 5,
                'volume_id' => 6
            ],
            [
                'paper_id' => 3,
                'weight_id' => 6,
                'volume_id' => 6
            ],
            [
                'paper_id' => 3,
                'weight_id' => 9,
                'volume_id' => 6
            ],
            [
                'paper_id' => 3,
                'weight_id' => 10,
                'volume_id' => 7
            ],
            [
                'paper_id' => 3,
                'weight_id' => 11,
                'volume_id' => 7
            ],
            [
                'paper_id' => 3,
                'weight_id' => 12,
                'volume_id' => 8
            ],
            [
                'paper_id' => 4,
                'weight_id' => 5,
                'volume_id' => 3
            ],
            [
                'paper_id' => 4,
                'weight_id' => 6,
                'volume_id' => 3
            ],
            [
                'paper_id' => 4,
                'weight_id' => 9,
                'volume_id' => 1
            ],
            [
                'paper_id' => 4,
                'weight_id' => 10,
                'volume_id' => 2
            ],
            [
                'paper_id' => 4,
                'weight_id' => 11,
                'volume_id' => 3
            ],
            [
                'paper_id' => 4,
                'weight_id' => 12,
                'volume_id' => 4
            ],
            [
                'paper_id' => 5,
                'weight_id' => 6,
                'volume_id' => 16
            ],
            [
                'paper_id' => 5,
                'weight_id' => 9,
                'volume_id' => 17
            ],
            [
                'paper_id' => 6,
                'weight_id' => 3,
                'volume_id' => 19
            ],
            [
                'paper_id' => 6,
                'weight_id' => 4,
                'volume_id' => 19
            ],
            [
                'paper_id' => 6,
                'weight_id' => 5,
                'volume_id' => 19
            ],
            [
                'paper_id' => 6,
                'weight_id' => 6,
                'volume_id' => 19
            ],
            [
                'paper_id' => 6,
                'weight_id' => 8,
                'volume_id' => 19
            ],
            [
                'paper_id' => 6,
                'weight_id' => 10,
                'volume_id' => 19
            ],
            [
                'paper_id' => 7,
                'weight_id' => 3,
                'volume_id' => 19
            ],
            [
                'paper_id' => 7,
                'weight_id' => 4,
                'volume_id' => 19
            ],
            [
                'paper_id' => 7,
                'weight_id' => 5,
                'volume_id' => 19
            ],
            [
                'paper_id' => 7,
                'weight_id' => 6,
                'volume_id' => 19
            ],
            [
                'paper_id' => 7,
                'weight_id' => 8,
                'volume_id' => 19
            ],
            [
                'paper_id' => 7,
                'weight_id' => 10,
                'volume_id' => 19
            ],
            [
                'paper_id' => 8,
                'weight_id' => 3,
                'volume_id' => 19
            ],
            [
                'paper_id' => 8,
                'weight_id' => 4,
                'volume_id' => 19
            ],
            [
                'paper_id' => 8,
                'weight_id' => 5,
                'volume_id' => 19
            ],
            [
                'paper_id' => 8,
                'weight_id' => 6,
                'volume_id' => 19
            ],
            [
                'paper_id' => 8,
                'weight_id' => 8,
                'volume_id' => 19
            ],
            [
                'paper_id' => 8,
                'weight_id' => 10,
                'volume_id' => 19
            ],
            [
                'paper_id' => 9,
                'weight_id' => 3,
                'volume_id' => 18
            ],
            [
                'paper_id' => 9,
                'weight_id' => 4,
                'volume_id' => 18
            ],
            [
                'paper_id' => 9,
                'weight_id' => 6,
                'volume_id' => 18
            ],
            [
                'paper_id' => 9,
                'weight_id' => 4,
                'volume_id' => 20
            ],
            [
                'paper_id' => 10,
                'weight_id' => 3,
                'volume_id' => 18
            ],
            [
                'paper_id' => 10,
                'weight_id' => 4,
                'volume_id' => 18
            ],
            [
                'paper_id' => 11,
                'weight_id' => 3,
                'volume_id' => 18
            ],
            [
                'paper_id' => 11,
                'weight_id' => 4,
                'volume_id' => 18
            ],
            [
                'paper_id' => 12,
                'weight_id' => 3,
                'volume_id' => 18
            ],
            [
                'paper_id' => 12,
                'weight_id' => 4,
                'volume_id' => 18
            ],
            [
                'paper_id' => 13,
                'weight_id' => 3,
                'volume_id' => 18
            ],
            [
                'paper_id' => 13,
                'weight_id' => 4,
                'volume_id' => 18
            ],
            [
                'paper_id' => 10,
                'weight_id' => 5,
                'volume_id' => 15
            ],
            [
                'paper_id' => 10,
                'weight_id' => 7,
                'volume_id' => 15
            ],
            [
                'paper_id' => 10,
                'weight_id' => 8,
                'volume_id' => 15
            ],
            [
                'paper_id' => 10,
                'weight_id' => 10,
                'volume_id' => 15
            ],
            [
                'paper_id' => 10,
                'weight_id' => 11,
                'volume_id' => 15
            ],
            [
                'paper_id' => 10,
                'weight_id' => 12,
                'volume_id' => 15
            ],
            [
                'paper_id' => 11,
                'weight_id' => 5,
                'volume_id' => 15
            ],
            [
                'paper_id' => 11,
                'weight_id' => 7,
                'volume_id' => 15
            ],
            [
                'paper_id' => 11,
                'weight_id' => 8,
                'volume_id' => 15
            ],
            [
                'paper_id' => 11,
                'weight_id' => 10,
                'volume_id' => 15
            ],
            [
                'paper_id' => 11,
                'weight_id' => 11,
                'volume_id' => 15
            ],
            [
                'paper_id' => 11,
                'weight_id' => 12,
                'volume_id' => 15
            ],
            [
                'paper_id' => 12,
                'weight_id' => 5,
                'volume_id' => 15
            ],
            [
                'paper_id' => 12,
                'weight_id' => 7,
                'volume_id' => 15
            ],
            [
                'paper_id' => 12,
                'weight_id' => 8,
                'volume_id' => 15
            ],
            [
                'paper_id' => 12,
                'weight_id' => 10,
                'volume_id' => 15
            ],
            [
                'paper_id' => 12,
                'weight_id' => 11,
                'volume_id' => 15
            ],
            [
                'paper_id' => 12,
                'weight_id' => 12,
                'volume_id' => 15
            ],
            [
                'paper_id' => 13,
                'weight_id' => 5,
                'volume_id' => 15
            ],
            [
                'paper_id' => 13,
                'weight_id' => 7,
                'volume_id' => 15
            ],
            [
                'paper_id' => 13,
                'weight_id' => 8,
                'volume_id' => 15
            ],
            [
                'paper_id' => 13,
                'weight_id' => 10,
                'volume_id' => 15
            ],
            [
                'paper_id' => 13,
                'weight_id' => 11,
                'volume_id' => 15
            ],
            [
                'paper_id' => 13,
                'weight_id' => 12,
                'volume_id' => 15
            ],
            [
                'paper_id' => 14,
                'weight_id' => 1,
                'volume_id' => 21
            ],
            [
                'paper_id' => 14,
                'weight_id' => 2,
                'volume_id' => 21
            ],
            [
                'paper_id' => 14,
                'weight_id' => 3,
                'volume_id' => 21
            ]
        ];

        DB::table('paper_weight_volume')->insert($data);
    }
}
