<?php

namespace JanRejnowski\PapersLibrary\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlterVolumeValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = collect([
            [
                'id' => 1,
                'value' => 0.70
            ],
            [
                'id' => 2,
                'value' => 0.71
            ],
            [
                'id' => 3,
                'value' => 0.72
            ],
            [
                'id' => 4,
                'value' => 0.73
            ],
            [
                'id' => 5,
                'value' => 0.74
            ],
            [
                'id' => 6,
                'value' => 0.82
            ],
            [
                'id' => 7,
                'value' => 0.83
            ],
            [
                'id' => 8,
                'value' => 0.84
            ],
            [
                'id' => 9,
                'value' => 0.96
            ],
            [
                'id' => 10,
                'value' => 0.97
            ],
            [
                'id' => 11,
                'value' => 0.98
            ],
            [
                'id' => 12,
                'value' => 0.99
            ],
            [
                'id' => 13,
                'value' => 1.00
            ],
            [
                'id' => 14,
                'value' => 1.10
            ],
            [
                'id' => 15,
                'value' => 1.13
            ],
            [
                'id' => 16,
                'value' => 1.20
            ],
            [
                'id' => 17,
                'value' => 1.25
            ],
            [
                'id' => 18,
                'value' => 1.30
            ],
            [
                'id' => 19,
                'value' => 1.50
            ],
            [
                'id' => 20,
                'value' => 1.95
            ],
            [
                'id' => 21,
                'value' => 2.00
            ]
        ]);

        $now = \Carbon\Carbon::now();
        $cases = $data->map(function($item) {
            return "WHEN {$item['id']} THEN {$item['value']}";
        })->implode(' ');

        DB::table('volumes')->whereIn('id', $data->pluck('id')->toArray())->update([
            'value' => \DB::raw("CASE `id` {$cases} END"),
            'updated_at' => $now
        ]);
        
    }
}
