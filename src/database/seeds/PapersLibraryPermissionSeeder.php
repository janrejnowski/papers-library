<?php
/**
 * Created by PhpStorm.
 * User: Jan Rejnowski
 * Date: 22.02.2018
 * Time: 09:49
 */

namespace JanRejnowski\PapersLibrary\Database\Seeds;

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

class PapersLibraryPermissionSeeder extends Seeder
{
    public function permissions() : array
    {
        return [
            [
                'name' => 'papers-library',
                'description' => 'Display papers menu',
            ],
            [
                'name' => 'papers-library-papers',
                'description' => 'View, create, edit and delete papers',
            ],
            [
                'name' => 'papers-library-weight',
                'description' => 'View, create, edit and delete papers weights',
            ],
            [
                'name' => 'papers-library-volume',
                'description' => 'View, create, edit and delete papers volumes',
            ],
        ];
    }

    public function run()
    {
        $admin = Role::where('name', 'admin')->first();

        foreach ($this->permissions() as $value) {
            $perm = Permission::updateOrCreate(
                ['name' => $value['name']],
                ['name' => $value['name'], 'description' => $value['description']]
            );


            if ($admin !== null && $admin->hasPermission($perm->name) === false) {
                $admin->attachPermission($perm);
            }
        }
    }

    public function down()
    {
        Permission::whereIn('name', array_column($this->permissions(), 'name'))->delete();
    }
}