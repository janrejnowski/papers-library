<?php

namespace JanRejnowski\PapersLibrary\Database\Seeds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class WeightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 60
            ],
            [
                'id'=>2,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 70
            ],
            [
                'id' => 3,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 80
            ],
            [
                'id' => 4,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 90
            ],
            [
                'id' => 5,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 100
            ],
            [
                'id' => 6,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 115
            ],
            [
                'id' => 7,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 120
            ],
            [
                'id' => 8,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 130
            ],
            [
                'id' => 9,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 135
            ],
            [
                'id' => 10,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 150
            ],
            [
                'id' => 11,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 170
            ],
            [
                'id' => 12,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 200
            ]
        ];
        DB::table('weights')->insert($data);
    }
}
