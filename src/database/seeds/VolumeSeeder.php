<?php

namespace JanRejnowski\PapersLibrary\Database\Seeds;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VolumeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.70
            ],
            [
                'id' => 2,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.71
            ],
            [
                'id' => 3,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.72
            ],
            [
                'id' => 4,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.73
            ],
            [
                'id' => 5,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.74
            ],
            [
                'id' => 6,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.82
            ],
            [
                'id' => 7,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.83
            ],
            [
                'id' => 8,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.84
            ],
            [
                'id' => 9,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.96
            ],
            [
                'id' => 10,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.97
            ],
            [
                'id' => 11,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.98
            ],
            [
                'id' => 12,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 0.99
            ],
            [
                'id' => 13,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.00
            ],
            [
                'id' => 14,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.10
            ],
            [
                'id' => 15,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.13
            ],
            [
                'id' => 16,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.20
            ],
            [
                'id' => 17,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.25
            ],
            [
                'id' => 18,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.30
            ],
            [
                'id' => 19,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.50
            ],
            [
                'id' => 20,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 1.95
            ],
            [
                'id' => 21,
                'created_at' => '2018-01-25 12:00:22',
                'name' => 2.00
            ]
        ];

        DB::table('volumes')->insert($data);
    }
}
