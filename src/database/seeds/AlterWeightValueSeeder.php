<?php

namespace JanRejnowski\PapersLibrary\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AlterWeightValueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = collect([
            [
                'id' => 1,
                'value' => 60
            ],
            [
                'id'=>2,
                'value' => 70
            ],
            [
                'id' => 3,
                'value' => 80
            ],
            [
                'id' => 4,
                'value' => 90
            ],
            [
                'id' => 5,
                'value' => 100
            ],
            [
                'id' => 6,
                'value' => 115
            ],
            [
                'id' => 7,
                'value' => 120
            ],
            [
                'id' => 8,
                'value' => 130
            ],
            [
                'id' => 9,
                'value' => 135
            ],
            [
                'id' => 10,
                'value' => 150
            ],
            [
                'id' => 11,
                'value' => 170
            ],
            [
                'id' => 12,
                'value' => 200
            ]
        ]);

        $now = \Carbon\Carbon::now();
        $cases = $data->map(function($item) {
            return "WHEN {$item['id']} THEN {$item['value']}";
        })->implode(' ');

        DB::table('weights')->whereIn('id', $data->pluck('id')->toArray())->update([
            'value' => \DB::raw("CASE `id` {$cases} END"),
            'updated_at' => $now
        ]);
        
    }
}
