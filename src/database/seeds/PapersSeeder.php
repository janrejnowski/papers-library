<?php

namespace JanRejnowski\PapersLibrary\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PapersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Amber Graphic',
                'long_description' => 'Biały papier offsetowy Standard',
                'public' => 1,
            ],
            [
                'id' => 2,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'G-Print',
                'long_description' => 'Matowy papier powlekany Premium',
                'public' => 1,
            ],
            [
                'id' => 3,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Magno Satin',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 4,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Magno Gloss',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 5,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Sora Matt Plus',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 6,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Alto',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 7,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Munken Print Cream',
                'long_description' => 'Kremowy papier książkowy Premium',
                'public' => 1,
            ],
            [
                'id' => 8,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Munken Print White',
                'long_description' => 'Biały papier książkowy Premium',
                'public' => 1,
            ],
            [
                'id' => 9,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Munken Premium Cream',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 10,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Munken Lynx',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 11,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Munken Pure',
                'long_description' => '',
                'public' => 1,


            ],
            [
                'id' => 12,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Munken Polar',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 13,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Munken Kristal',
                'long_description' => '',
                'public' => 1,
            ],
            [
                'id' => 14,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Creamy',
                'long_description' => 'Kremowy papier książkowy Standard',
                'public' => 1,
            ],
            [
                'id' => 15,
                'created_at' => '2018-01-26 07:06:15',
                'slug' => 'volumed',
                'name' => 'Volumed',
                'long_description' => 'Papier objętościowy',
                'public' => 0,
            ]
        ];

        DB::table('papers')->insert($data);
    }
}
