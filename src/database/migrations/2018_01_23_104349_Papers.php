<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Papers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            Schema::create('papers', function (Blueprint $table){
                $table->increments('id');
                $table->timestamps();
                $table->string('slug');
                $table->string('name');
                $table->text('long_description');
                $table->tinyInteger('public')->default(0);
                $table->softDeletes();
            });

            Schema::create('weights', function (Blueprint $table){
                $table->increments('id');
                $table->timestamps();
                $table->string('name')->unique();
                $table->softDeletes();
            });

            Schema::create('volumes', function (Blueprint $table){
                $table->increments('id');
                $table->timestamps();
                $table->decimal('name', 10)->unique();
                $table->softDeletes();
            });

            Schema::create('paper_weight_volume', function (Blueprint $table){
                $table->integer('paper_id')->unsigned();
                $table->integer('weight_id')->unsigned();
                $table->integer('volume_id')->unsigned();

                $table->foreign('paper_id')->references('id')->on('papers')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('weight_id')->references('id')->on('weights')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('volume_id')->references('id')->on('volumes')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['paper_id', 'weight_id', 'volume_id']);

            });

        } catch (PDOException $ex){
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\PapersLibrary\Database\Seeds\PapersSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\PapersLibrary\Database\Seeds\WeightSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\PapersLibrary\Database\Seeds\VolumeSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\PapersLibrary\Database\Seeds\PaperWeightVolumeSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\PapersLibrary\Database\Seeds\PapersLibraryPermissionSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('paper_weight_volume');
        Schema::drop('papers');
        Schema::drop('weights');
        Schema::drop('volumes');

        $permissions = new \JanRejnowski\PapersLibrary\Database\Seeds\PapersLibraryPermissionSeeder();
        $permissions->down();
    }
}
