<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PapersAlter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws \PDOException
     */
    public function up()
    {
        try {
            Schema::table('weights', function (Blueprint $table){
                $table->string('value')->after('name');
                $table->tinyInteger('active')->default(1)->after('value');
            });

            Schema::table('volumes', function (Blueprint $table){
                $table->dropUnique('volumes_name_unique');
                $table->decimal('value', 10)->after('name');
                $table->tinyInteger('active')->default(1)->after('value');
            });


        } catch (PDOException $ex){
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\PapersLibrary\Database\Seeds\AlterWeightValueSeeder::class,
        ]);
        Artisan::call('db:seed', [
            '--class' => \JanRejnowski\PapersLibrary\Database\Seeds\AlterVolumeValueSeeder::class,
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('weights', function(Blueprint $table)
        {
            $table->dropColumn(['value', 'active']);
        });

        Schema::table('volumes', function(Blueprint $table)
        {
            $table->dropColumn(['value', 'active']);
        });
    }
}
