<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaperVolumeWeightDefault extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws \PDOException
     */
    public function up()
    {
        try {
            Schema::table('paper_weight_volume', function (Blueprint $table){
                $table->tinyInteger('default')->default(0)->after('volume_id');
            });
        } catch (PDOException $ex){
            $this->down();
            throw $ex;
        }
    }

    public function down()
    {
        Schema::table('paper_weight_volume', function(Blueprint $table)
        {
            $table->dropColumn('default');
        });
    }
}
