<?php

namespace JanRejnowski\PapersLibrary\Repositories;

use App\Repositories\Contracts\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use JanRejnowski\PapersLibrary\Model\Paper;
use JanRejnowski\PapersLibrary\Model\Volume;

/**
 * Class PaperRepository
 * @package JanRejnowski\PapersLibrary\Repositories
 *
 * @property    \JanRejnowski\PapersLibrary\Model\Paper|\Illuminate\Database\Eloquent\Builder $model
 */
class PaperRepository extends Repository
{
    public function model() : string
    {
        return Paper::class;
    }

    public function findId(int $id = 0, array $columns = ['*'])
    {
        $data = $this->model->with('weight','weight.volume')->withTrashed()->find($id, $columns);

        if ($id === 0) {
            return $this->error(400, __('No paper id have been given.'));
        }

        if ($data === null) {
            return $this->error(400, __('Given id :code is invalid or paper not exist.', ['code' => $id]));
        }

        if ($data->deleted_at !== null) {
            return $this->error(400, __('Paper with given id :code is deleted.', ['code' => $id]));
        }

        return $data;
    }

    /**
     * @param   Request     $request
     * @param   int     $id
     * @return  Paper
     * @throws \Exception
     * @throws \Throwable
     */
    public function store(Request $request, int $id = 0) : Paper
    {
        $paper = ($id === 0) ? $this->model : $this->find($id);

        $paper->name = $request->input('name');
        $paper->long_description = $request->input('long_description');
        $paper->slug = $request->input('slug');
        $paper->public = $request->input('public', 0);

        DB::transaction(function () use ($paper, $id, $request) {
            if ($id > 0) {
                $paper->weight()->sync([]);
            }
            $paper->save();
            $paper->weight()->attach($request->input('weight_volume'));
        });

        return $paper;
    }

    public function getPapers() : \Illuminate\Support\Collection
    {
        return $this->model->with(['weight', 'weight.volume'])->get()->keyBy('id');
    }

    public function getPublicPapers() : \Illuminate\Support\Collection
    {
        return $this->model->with(['weight', 'weight.volume'])->where('public',1)->get()->keyBy('id');
    }

    public function getPapersForJson() : \Illuminate\Support\Collection
    {
        return $this->getPapers()->transform( function (Paper $paper){
            $paper->params->transform(function (array $item){
                $item['weight']->setAppends(['text', 'default'])->makeHidden('name');
                $item['volumes']->transform( function (Volume $volume){
                    return $volume->setAppends(['text'])->makeHidden('name');
                });
                return $item;
            });
            return $paper;
        });
    }

    public function getPublicPapersForJson() : \Illuminate\Support\Collection
    {
        return $this->getPublicPapers()->transform( function (Paper $paper){
            $paper->params->transform(function (array $item){
                $item['weight']->setAppends(['text', 'default'])->makeHidden('name');
                $item['volumes']->transform( function (Volume $volume){
                    return $volume->setAppends(['text'])->makeHidden('name');
                });
                return $item;
            });
            return $paper;
        });
    }

    public function getSimplerPublicPapersForJson() : \Illuminate\Support\Collection
    {
        return $this->getPublicPapers()->transform( function (Paper $paper){
            $paper->makeHidden(['slug', 'name', 'long_description', 'public']);
            $paper->params->transform(function (array $item){
                $item['weight']->setAppends(['text', 'default'])->makeHidden(['active', 'name']);
                $item['volumes']->transform( function (Volume $volume){
                    return $volume->setAppends(['text'])->makeHidden(['active', 'name']);
                });
                return $item;
            });
            return $paper;
        });
    }

    public function getPaper(string $slug)
    {
        return $this->model->with(['weight', 'weight.volume'])->where('slug', $slug)->first();
    }

    public function getPublicPaper(string $slug)
    {
        return $this->model->with(['weight', 'weight.volume'])->where('public',1)->where('slug', $slug)->first();
    }

    public function destroy(int $id)
    {
        $data = $this->findId($id);

        return $data instanceof MessageBag ? $data : collect($data->delete());
    }

    public function edit(Request $request, int $id)
    {
        $data = $this->findId($id);

        return $data instanceof MessageBag ? $data : $data->$this->store($request, $id);
    }

    public function all(array $columns = ['*']): \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->orderBy('name')->get($columns);
    }
}