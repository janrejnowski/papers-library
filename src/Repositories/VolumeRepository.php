<?php

namespace JanRejnowski\PapersLibrary\Repositories;

use App\Repositories\Contracts\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use JanRejnowski\PapersLibrary\Model\Volume;

/**
 * Class VolumeRepository
 * @package JanRejnowski\PapersLibrary\Repositories
 *
 * @property    \JanRejnowski\PapersLibrary\Model\Volume|\Illuminate\Database\Eloquent\Builder $model
 */
class VolumeRepository extends Repository
{
    public function model() : string
    {
        return Volume::class;
    }

    public function store(Request $request, int $id = 0)
    {
        $volume = ($id === 0) ? $this->model : $this->find($id);

        $volume->name = $request->input('name');
        $volume->value = $request->input('value');
        $volume->active = $request->input('active', 0);
        $volume->save();

        return $volume;
    }

    public function findId(int $id = 0, array $columns = ['*'])
    {
        $data = $this->model->withTrashed()->find($id, $columns);

        if ($id === 0) {
            return $this->error(400, __('No volume id have been given.'));
        }

        if ($data === null) {
            return $this->error(400, __('Given id :code is invalid or volume not exist.', ['code' => $id]));
        }

        if ($data->deleted_at !== null) {
            return $this->error(400, __('Volume with given id :code is deleted.', ['code' => $id]));
        }

        return $data;
    }

    public function destroy(int $id = 0)
    {
        $data = $this->findId($id);

        return $data instanceof MessageBag ? $data : collect($data->delete());
    }

    public function all(array $columns = ['*']): \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->orderBy('name')->get($columns);
    }
}