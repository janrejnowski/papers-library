<?php

namespace JanRejnowski\PapersLibrary\Repositories;

use App\Repositories\Contracts\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use JanRejnowski\PapersLibrary\Model\Weight;

/**
 * Class WeightRepository
 * @package JanRejnowski\PapersLibrary\Repositories
 *
 * @property    \JanRejnowski\PapersLibrary\Model\Weight|\Illuminate\Database\Eloquent\Builder $model
 */
class WeightRepository extends Repository
{
    public function model() : string
    {
        return Weight::class;
    }

    public function store(Request $request, int $id = 0)
    {
        $weight = ($id === 0) ? $this->model : $this->find($id);

        $weight->name = $request->input('name');
        $weight->value = $request->input('value');
        $weight->active = $request->input('active', 1);
        $weight->save();

        return $weight;
    }

    public function findId(int $id = 0, array $columns = ['*'])
    {
        $data = $this->model->withTrashed()->find($id, $columns);

        if ($id === 0) {
            return $this->error(400, __('No weight id have been given.'));
        }

        if ($data === null) {
            return $this->error(400, __('Given id :code is invalid or weight not exist.', ['code' => $id]));
        }

        if ($data->deleted_at !== null) {
            return $this->error(400, __('Weight with given id :code is deleted.', ['code' => $id]));
        }

        return $data;
    }

    public function destroy(int $id = 0)
    {
        $data = $this->findId($id);

        return $data instanceof MessageBag ? $data : collect($data->delete());
    }

    public function all(array $columns = ['*']): \Illuminate\Database\Eloquent\Collection
    {
        return $this->model->orderByRaw('LENGTH(name)')->get($columns);
    }
}