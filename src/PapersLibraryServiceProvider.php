<?php

namespace JanRejnowski\PapersLibrary;

use Illuminate\Support\ServiceProvider;

class PapersLibraryServiceProvider extends ServiceProvider
{
    const NAMESPACE = 'papers-library';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layout.home', function (\Illuminate\View\View $view) {
            $this->makeMenu($view);
        });

        $this->loadViewsFrom(__DIR__ . '/resources/views', self::NAMESPACE);
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadJsonTranslationsFrom(__DIR__ . '/resources/lang');

        $this->publishes([
            __DIR__ . '/database/migrations' => database_path('migrations'),
            __DIR__ . '/resources/lang' => resource_path('lang/vendor/'.self::NAMESPACE),
            __DIR__ . '/config/config.php' => config_path(self::NAMESPACE.'.php')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->mergeConfigFrom(__DIR__ . '/config/config.php', self::NAMESPACE);
    }

    private function makeMenu(\Illuminate\View\View $view)
    {
        $data = make_menu(config(self::NAMESPACE.'.menu'), $view->offsetGet('currentUser'));

        if ($data){
            if ($view->offsetGet('menu')->contains('route', self::NAMESPACE)){
                $view->offsetGet('menu')->map(function ($item) use($data){
                   if($item->route === self::NAMESPACE){
                       $item = $data;
                   }
                   return $item;
                });
            } else {
                $view->offsetSet('menu', $view->offsetGet('menu')->merge(collect($data)));
            }
        }
    }
}
