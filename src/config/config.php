<?php

return [

    'menu' => [
        [
            'key' => 'papers-library',
            'name' => 'Papers',
            'route' => 'papers',
            'route-prefix' => 'papers',
            'icon-class' => 'fa-sticky-note',
            'children' => [
                [
                    'key' => 'papers-library-papers',
                    'name' => 'Papers list',
                    'route' => 'papers-library.paper.index',
                    'route-prefix' => 'papers',
                    'icon-class' => 'fa-sort-amount-asc',
                    'children' => []
                ],
                [
                    'key' => 'papers-library-weight',
                    'name' => 'Weight',
                    'route' => 'papers-library.weight.index',
                    'route-prefix' => 'weights',
                    'icon-class' => 'fa-tint',
                    'children' => [],
                ],
                [
                    'key' => 'papers-library-volume',
                    'name' => 'Volume',
                    'route' => 'papers-library.volume.index',
                    'route-prefix' => 'volumes',
                    'icon-class' => 'fa-map',
                    'children' => [],
                ]
            ]
        ]
    ]
];